export function fetchJSON(url: RequestInfo, options?: RequestInit) {
  return (
    fetch(url, options)
      .then(res =>
        res.text().then(text => {
          try {
            let json = JSON.parse(text)
            if (json.error || (200 <= res.status && res.status < 300)) {
              return json
            }
            return { error: text }
          } catch (error) {
            // e.g. when got 404 html response
            return { error: text }
          }
        }),
      )
      // e.g. when network connection failed
      .catch(error => ({ error: String(error) }))
  )
}
