import express, { Request } from 'express'
import { UserService } from './user.service'
import { HttpError, wrapControllerMethod } from './error'

export class UserController {
  routes = express.Router()

  constructor(public userService: UserService) {
    this.routes.get('/profile/:id', wrapControllerMethod(this.getProfile))
  }

  getProfile = async (req: Request) => {
    let id = +req.params.id
    if (!id) throw new HttpError(400, 'invalid id in req.params')
    return await this.userService.getProfile(id)
  }
}
