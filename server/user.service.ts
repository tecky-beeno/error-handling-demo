import { Knex } from 'knex'
import { HttpError } from './error'

export class UserService {
  constructor(public knex: Knex) {}

  async getProfile(id: number) {
    let user = await this.knex
      .select<{
        username: string
        picture: string | null
        bio: string | null
      }>('username', 'picture', 'bio')
      .from('user')
      .where({ id })
      .first()
    if (!user) throw new HttpError(404, 'User not found')
    return user
  }
}
