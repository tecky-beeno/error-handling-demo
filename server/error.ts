import express, { Request, Response, NextFunction } from 'express'

export class HttpError extends Error {
  constructor(public status: number, public message: string) {
    super()
  }

  static end(res: express.Response, error: any) {
    let e: HttpError | null = error
    let status: number = e?.status || 500
    let message: string = e?.message || String(error)
    res.status(status).json({ error: message })
  }
}

export function wrapControllerMethod<T>(fn: (req: Request) => T | Promise<T>) {
  return async (req: Request, res: Response) => {
    try {
      let json: T = await fn(req)
      res.json(json)
    } catch (error) {
      HttpError.end(res, error)
    }
  }
}
